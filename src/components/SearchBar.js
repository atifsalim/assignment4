import React from 'react';

const SearchBar = ({nameChangeHandler, addressChangeHandler}) => {
    return (
        <div className="description">
            <span  style={{ marginLeft: "20px",fontSize:"15px", fontWeight:"bold" }}>What : </span>
            <input
                type="text"
                placeholder="Jobs title, keyword or company"
                className="ui input"
                onChange={(e) => nameChangeHandler(e.target.value)}
            />
            <span style={{ marginLeft: "20px",fontSize:"15px", fontWeight:"bold" }}>Where :  </span>
            <input
                type="text"
                placeholder="Enter city"
                className="ui input"
                onChange={(e) => addressChangeHandler(e.target.value)}
            />
            <button className="ui primary button" style={{ marginLeft: "20px" }}>Find Jobs</button>

        </div>
    );
}

export default SearchBar;